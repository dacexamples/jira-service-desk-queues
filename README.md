# JIRA Service Desk - Custom Queues add-on example.

Welcome to this add-on example.

This add-on example shows you how you can add Custom Queues to the JIRA Service Desk Agent Portal.

We are using the ACE ([Atlassian Connect Express](https://bitbucket.org/atlassian/atlassian-connect-express)) framework. 

## Want to know more about custom queues in JIRA Service Desk?

[Adding your Custom Queues to JIRA Service Desk](https://developer.atlassian.com/jiracloud/jira-service-desk-modules-agent-view-39988009.html#JIRAServiceDeskmodules-Agentview-Queuegroup).